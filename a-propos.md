Diaporama réalisé par XXXX.

Hébergé sur la forge gérée par l'[AEIF](https://aeif.fr).

<!--

Choisissez ci-dessous la licence de votre diaporama
en ajoutant un x entre les crochets de votre choix :

[x] Tout droit réservé
[ ] CC BY NC ND
[ ] CC BY ND
[ ] CC BY NC SA
[ ] CC BY SA	
[ ] CC BY NC	
[ ] CC BY	
[ ] CC ZERO	

Pour savoir quelle licence choisir, consultez ce tableau :
https://link.infini.fr/licencescc

La licence choisie sera automatiquement affichée dans
l'onglet "A propos" du menu de votre diaporama.

-->
