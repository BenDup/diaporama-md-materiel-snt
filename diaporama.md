## MATERIEL et PREREQUIS
### nécessaires pour passer une bonne année de 2de en **SNT**
 
Cliquez sur la flèche en bas à droite pour passer à la diapositive suivante. <br>
Vous pouvez aussi appuyer sur la touche espace ou sur la flèche droite de votre clavier.

---

*Lorsqu’on vient en cours de SNT, on doit avoir comme pour la plupart des matières, un minimum de matériel scolaire comme :*

1.	de quoi écrire (stylos, crayon, gomme, correcteur…) et brouillon ;
2.	règle ou équerre ;
3.	calculatrice ;
4.  ...et le matériel électronique ci-après :

---

## 1. Votre ordinateur portable

...protégé (housse) et chargé, et avec le chargeur les grosses journées et les journées d’interro SNT. <br>
Cette condition, COMME LES AUTRES d’ailleurs, rentre dans les **évaluations** : la pénalité en cas d’ordi inopérant peut être lourde sur la note selon le type d’évaluation. En 2de, on doit apprendre à être autonome, et cela passe par le fait de savoir gérer ses affaires correctement !

---

## 2. Des écouteurs

Au cours des séances et des interros, vous êtes amenés à visionner de nombreux extraits vidéos : il n’est pas question de déranger le voisin, qui va à son rythme et pas forcément avec les mêmes vidéos ! <br>
ATTENTION, les écouteurs à prise Mac-iPhone ne sont pas compatibles avec votre PC… et les écouteurs bluetooth sont pratiques mais peuvent planter ou interférer…
C’est pourquoi il est demandé d’avoir des écouteurs basiques avec une prise « jack 3,5mm » vu le prix totalement dérisoire (à partir d'un euro en supermarché).

---

Exemple d' [écouteurs jack 3.5 de LUXE (très bon son)](https://amzn.to/30jbR2w) pour les plus exigeants, à 8 euros :

![](reveal.js/plugin/chalkboard/img/ecout8eur.png)

---

## 3. Clé USB (au moins une de 32Go)

Essentiel pour se repasser les fichiers à la fin des séances de travaux en groupes, mais aussi pour garder une copie de sauvegarde de ses fichiers en cas de plantage grave de l’ordi (ou casse, ou vol etc).

---

**ATTENTION**, la concurrence des prix est rude sur le marché des clés usb, donc les constructeurs tirent sur les coûts au maximum en produisant du matériel pourri : la plupart des clés USB sont très lentes, buguent souvent et vieillissent mal (avec perte définitive des données). <br>
Pour en avoir testé des centaines, voici quelques modèles de clés, chez divers constructeurs, qui sortent du lot (statistiquement) mais qui sont un peu plus chers. Trois exemples très différents, *largement testés (clic droit -> ouvrir dans un nouvel onglet)* :

---

### 3.1 Clé Kingston

Clé Kingston : petite, vitesse moyenne mais fiable et clé métallique robuste, peu chère. [Modèle DTMC3G2](https://amzn.to/3TgVRJw)

![](reveal.js/plugin/chalkboard/img/kingston-metal_1.jpg)

---

### 3.2 Clé Sandisk

Clé SanDisk : Plus grosse mais avec une astuce géniale : chaque bout de la clé est un connecteur (ça fonctionne dans un port USB-A classique et dans un USB-C comme les smartphones). [Modèle Ultra Dual Drive Luxe](https://amzn.to/4cTFAkE)

![](reveal.js/plugin/chalkboard/img/sandisk-metal-dual_1.jpg)

---

### 3.3 Clé Corsair

Clé Corsair Voyager : version caoutchoutée très résistante (plus volumineuse). [Modèle Voyager](https://amzn.to/3Tj2NFU)

![](reveal.js/plugin/chalkboard/img/corsair-voyager.jpg)

---

### 3.4 Clé type SSD

Clé Kingston DataTraveler Max : le monstre ULTRA rapide (jusqu’à 5X la vitesse des autres). [Modèle DataTraveler Max](https://amzn.to/3MAtMt6)

![](reveal.js/plugin/chalkboard/img/K_DataTraveler_Max_1.jpg)

---

## 4. Identifiants divers

Il est hors de question d’être empêché de faire son travail parce qu’on accède pas au wifi ou aux machines fixes (codes réseau), à l’ENT (codes MBN), et aux services qui peuvent vous être utiles (cloud, mail, etc). Vous DEVEZ donc **retrouver facilement et rapidement vos identifiants**, que ce soit dans votre tête, sur votre carnet de correspondance, dans votre smartphone etc.

---

Cette année on verra beaucoup de choses :
- Internet, web, datas
- GPS, Photo num, Réseaux sociaux
- Objets connectés...

Du code ...

```python
def hello_world():
    print("Hello World !")
```

Et même des maths !

$$ {F}_{A/B} = G \times \frac{M_A M_B}{d^2} $$

